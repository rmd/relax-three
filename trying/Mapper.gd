
var height = 0
var width = 0

func _init(width, height):
	self.width = width
	self.height = height	

func cell_coord_to_graphic_coord(col, row):
	return Vector2(
		(width / 2) + width * col,
		(height / 2) + height * row,
	)

func graphic_coord_to_cell_coord(vector2):
	return {
		"row": 0,
		"col": 0
	}

func col_from_graphic_coord(vector2):
	return floor(vector2.x / width)
	
func row_from_graphic_coord(vector2):
	return floor(vector2.y / height)
