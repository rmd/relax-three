extends Node2D

const AnimatedPiece = preload("res://AnimatedPiece.gd")
const Piece = preload("res://Piece.gd")
const Matcher = preload("res://Matcher.gd")
const Mapper = preload("res://Mapper.gd")



#### heavily modelled after
##   https://www.youtube.com/watch?v=U2-JVhRLQTw
##   - helpful match three tutorial!


#var cols_in_grid = 6
#var rows_in_grid = 9
#var cols_in_grid = 12		#  480
#var rows_in_grid = 18		#  720
var cols_in_grid = 24		#  960
var rows_in_grid = 36		# 1440

var cells = []
var side_length = 40

var MAPPER = Mapper.new(side_length, side_length)

# seconds between ticks
const NORMAL = 0.15
const RHYTHMIC = 0.3
const SLOW = .5
const STOPPED = 600000

var time_threshold_reset = NORMAL

var selected_cell_scale_multiplier = 1.4

# initialized in ready()
var rng

var highlight
var selected_cell_vector = null

func position_for_vector2(vector2):
	return Vector2(
		(side_length/2) + side_length * vector2.x,
		(side_length/2) + side_length * vector2.y
	)

func swap_cells(a, b):
	var tmp = cells[a.x][a.y]
	
	cells[a.x][a.y] = cells[b.x][b.y]
	cells[b.x][b.y] = tmp

func select_cell(vector_of_cell_to_select):
	if(selected_cell_vector == null):
#		highlight.position = position_for_vector2(vector_of_cell_to_select)
#		highlight.visible = true
		
		selected_cell_vector = vector_of_cell_to_select
#		cells[selected_cell_vector.x][selected_cell_vector.y].visible = false

		var selected_cell = cells[selected_cell_vector.x][selected_cell_vector.y]
		
		var tween = get_tree().create_tween()
		tween.tween_property(
			selected_cell, 
			"scale", 
			Vector2(
				selected_cell_scale_multiplier,
				selected_cell_scale_multiplier
			), 
			.15
		)
	else:
		var selected_cell = cells[selected_cell_vector.x][selected_cell_vector.y]
		var tween = get_tree().create_tween()
		tween.tween_property(
			selected_cell, 
			"scale", 
			Vector2(
				1,
				1
			), 
			.15
		)
		
		# are they neighbours? 
		# if so swap them
		if(
			vector_of_cell_to_select.x == selected_cell_vector.x - 1 or
			vector_of_cell_to_select.x == selected_cell_vector.x + 1 or
			vector_of_cell_to_select.y == selected_cell_vector.y - 1 or
			vector_of_cell_to_select.y == selected_cell_vector.y + 1
		):
			swap_cells(selected_cell_vector, vector_of_cell_to_select)
		
		# no cell is selected now
		selected_cell_vector = null


var dragging_up = false
var dragging_down = false
var dragging_left = false
var dragging_right = false
func drag_swap(start_position, end_position):	
	set_drag_direction(start_position, end_position)

	var start_col = MAPPER.col_from_graphic_coord(start_position)
	var start_row = MAPPER.row_from_graphic_coord(start_position)
	
	if dragging_up:
		swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col, start_row - 1)
		)
		return
		
	if dragging_down:
		swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col, start_row + 1)
		)
		return
		
	if dragging_left:
		swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col - 1, start_row)
		)
		return

	if dragging_right:
		swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col + 1, start_row)
		)
		return
		
	# If not dragging at all, then select the tile
	select_cell(Vector2(start_col, start_row))
	
func set_drag_direction(start_position, end_position):	
	var start_col = MAPPER.col_from_graphic_coord(start_position)
	var end_col = MAPPER.col_from_graphic_coord(end_position)
	var start_row = MAPPER.row_from_graphic_coord(start_position)
	var end_row = MAPPER.row_from_graphic_coord(end_position)

	dragging_up = false
	dragging_down = false
	dragging_left = false
	dragging_right = false

	if(start_col < end_col): 
		dragging_right = true
		
	if(start_col > end_col):
		dragging_left = true
		
	if(start_row < end_row):
		dragging_down = true
		
	if(start_row > end_row):
		dragging_up = true




var moveableA
var moveableB
var originalA
var originalB
func visually_swap_cells(a, b):	
	var tween = get_tree().create_tween()
	
	if(cells[a.x][a.y] != originalA):
		originalA = cells[a.x][a.y]
		
		moveableA = originalA.duplicate()
		moveableA.position = originalA.position
		add_child(moveableA)
		
		originalA.visible = false
		var newAposition = MAPPER.cell_coord_to_graphic_coord(b.x, b.y)
		
		tween.tween_property(
			moveableA, 
			"scale", 
			Vector2(
				selected_cell_scale_multiplier,
				selected_cell_scale_multiplier
			), 
			.15
		)
		tween.tween_property(
			moveableA, 
			"position", 
			newAposition, 
			.1
		)
		
		
	if(cells[b.x][b.y] != originalB):	
		originalB = cells[b.x][b.y]
		
		moveableB = originalB.duplicate()
		moveableB.position = originalB.position
		add_child(moveableB)
		
		
		originalB.visible = false
		
		var newBposition = MAPPER.cell_coord_to_graphic_coord(a.x, a.y)
		tween.tween_property(
			moveableB, 
			"scale", 
			Vector2(
				selected_cell_scale_multiplier,
				selected_cell_scale_multiplier
			), 
			.15
		)	
	
		tween.tween_property(
			moveableB, 
			"position", 
			newBposition, 
			.05
		)
		
		

# these function names are pretty poor
func visually_swap_back():
	var tween = get_tree().create_tween()

#	tween.tween_property(
#		moveableA, 
#		"scale", 
#		Vector2.ONE, 
#		.15
#	)
#
#	tween.tween_property(
#		moveableB, 
#		"scale", 
#		Vector2.ZERO, 
#		.15
#	)	
##
	if(moveableA != null and originalA != null):
		tween.tween_property(
			moveableA, 
			"position", 
			originalA.position, 
			.05
		)

	if(moveableB != null and originalB != null):
		tween.tween_property(
			moveableB, 
			"position", 
			originalB.position, 
			.05
		)
	pass

func visually_restore_cells():
	var tween = get_tree().create_tween()

	tween.tween_property(
		moveableA, 
		"scale", 
		Vector2.ONE, 
		.15
	)

	tween.tween_property(
		moveableB, 
		"scale", 
		Vector2.ZERO, 
		.15
	)	
##
#	tween.tween_property(
#		moveableA, 
#		"position", 
#		originalA.position, 
#		.05
#	)
#
#	tween.tween_property(
#		moveableB, 
#		"position", 
#		originalB.position, 
#		.05
#	)

	if(originalA != null):
		originalA.visible = true

	if(originalB != null):
		originalB.visible = true

	if(moveableA != null):
		moveableA.visible = false

	if(moveableB != null):
		moveableB.visible = false

func visualize_drag(start_position, end_position):
	set_drag_direction(start_position, end_position)

	var start_col = MAPPER.col_from_graphic_coord(start_position)
	var start_row = MAPPER.row_from_graphic_coord(start_position)
		
	if dragging_up:
		print("up")
		visually_swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col, start_row - 1)
		)
		
		return
		
	if dragging_down:
		print("down")
		visually_swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col, start_row + 1)
		)
		return
		
	if dragging_left:
		print("left")
		visually_swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col - 1, start_row)
		)
		return

	if dragging_right:
		print("right")
		visually_swap_cells(
			Vector2(start_col, start_row),
			Vector2(start_col + 1, start_row)
		)
		return
		
	print("no direction")
	visually_swap_back()

var drag_origin = {}
var button_is_pressed = false
var drag_is_active = false
func _input(event):
	if event is InputEventMouseButton:
		button_is_pressed = event.is_pressed()
		if button_is_pressed:
			drag_origin = event.position
		else:
			# button has been released, stop drag and perform the swap
			drag_is_active = false
			drag_swap(drag_origin, event.position)
			visually_restore_cells()
	
	if event is InputEventMouseMotion:
		if button_is_pressed:
			# pressed button + movement = drag is active
			drag_is_active = true
			set_drag_direction(drag_origin, event.position)
			visualize_drag(drag_origin, event.position)
	else: 
		print(event)
		
func as_coord_string(col, row):
	return str("(", col, ", ", row, ")")

func random_piece():
	var animations = [
		"A", "B", "C", "D", "E", "F"
	]
	var animation = animations[
		rng.randi_range(0, animations.size() - 1)
	]
	
	var piece = get_node("../WhiteAll").duplicate()
	piece.choose_animation(
		animation,
		3
	)

	piece.visible = true
	return piece

func _ready():
	rng = RandomNumberGenerator.new()
		
	highlight = get_node("../Highlight")
	highlight = Piece.new(side_length, Color.WHITE)
	highlight.visible = false
	add_child(highlight)

	# initialize entire grid
	for col_index in cols_in_grid:
		cells.append([])
		
		for row_index in rows_in_grid:
			cells[col_index].append(null) 

#	# Uncomment this block to Populate the grid a column at a time
	for col_index in cols_in_grid:		
		for row_index in rows_in_grid:
			var piece = random_piece()
			cells[col_index][row_index] = piece

			while(Matcher.match_at(col_index, row_index, cells)) :
				piece = random_piece()
				cells[col_index][row_index] = piece

			add_child(piece)

	# Draw the grid
	draw_grid()

func draw_grid():
	for col_index in range(cells.size() - 1, -1, -1):
		for row_index in range(0, cells[col_index].size(), 1):
			if cells[col_index][row_index] != null:
				# ensure that the contents of the cell is in the right spot
				cells[col_index][row_index].position = position_for_vector2(Vector2(
					col_index,
					row_index
				))

func destroy(to_destroy):
		# Destroy all the pieces in matches
	for destroy in to_destroy:
		var col_index = destroy.x
		var row_index = destroy.y

		var thing = cells[col_index][row_index]
		
		cells[col_index][row_index] = null
		
		thing.get_parent().remove_child(thing)
		thing.queue_free()

func find_matches(): 
		# columns are drawn from the top to the bottom,
	# so the bottom cell is the highest index
	# draw cells and identify all pieces that are in a match and need
	# to be destroyed
	var to_destroy = []
	for col_index in range(cells.size() - 1, -1, -1):
		for row_index in range(0, cells[col_index].size(), 1):
			if(Matcher.match_at(col_index, row_index, cells)):
				to_destroy.append(Vector2(col_index, row_index))
				
	return to_destroy

func replace_destroyed_pieces():
	# Drop pieces down in their column
	# drop left-most column
	for col_index in range(cols_in_grid - 1, -1, -1):
		var row = cells[col_index]
		
		for row_index in range(rows_in_grid - 1, -1, -1):
			if cells[col_index][row_index] == null:
				if row_index == 0:
					var piece = random_piece()
					row[0] = piece
					add_child(piece)
					cells[col_index][row_index] = piece
					
				elif row_index > 0:
					cells[col_index][row_index] = cells[col_index][row_index - 1]
					cells[col_index][row_index - 1] = null

var time_threshold = time_threshold_reset
func _process(delta):
	time_threshold -= delta
	if time_threshold > 0:
		return

#	print("TICK")
	time_threshold = time_threshold_reset
	
	draw_grid()
	
	var to_destroy = find_matches()
	destroy(to_destroy)
	replace_destroyed_pieces()
