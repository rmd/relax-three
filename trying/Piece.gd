extends Node2D

var color = Color.BLACK

func _init(side_size, dominant_color):
	super()

	color = dominant_color

	var gradient = Gradient.new()
	gradient.offsets = [0.0]
	gradient.colors = [dominant_color]
	
	var texture = GradientTexture2D.new()
	texture.gradient = gradient
	
	var meshInstance = MeshInstance2D.new()
	meshInstance.mesh = BoxMesh.new()
	meshInstance.texture = texture 
	
	self.scale = Vector2(side_size, side_size)
	
	add_child(meshInstance)
