extends AnimatedSprite2D

var color = Color.BLACK

var SPRITE_SHEET = load("res://assets/sprites/spritesheet.png")

func cell_coords_to_rect2(cell_x, cell_y, side_size):
	# left, top, width, height
	return Rect2(
		side_size * cell_x,
		side_size * cell_y,
		side_size,
		side_size
	)

func start_texture():
	var start = AtlasTexture.new()
	start.atlas = SPRITE_SHEET
	
	return start
	
func _init(side_size, dominant_color):
	super()

	color = dominant_color

	self.sprite_frames = SpriteFrames.new()
	self.sprite_frames.add_animation("flashRed")
	self.sprite_frames.set_animation_loop("flashRed", true)
	print(self, "\ta:")
	var frame_0_texture = start_texture()
	frame_0_texture.region = cell_coords_to_rect2(0, 0, side_size)
	self.sprite_frames.add_frame("flashRed", frame_0_texture)
	
	print(self, "\tb:", frame_0_texture.region)
	var frame_1_texture = start_texture()
	frame_1_texture.region = cell_coords_to_rect2(0, 1, side_size)
	self.sprite_frames.add_frame("flashRed", frame_1_texture)
	print(self, "\tc:", frame_1_texture.region)
	
	var frame_2_texture = start_texture()
	frame_2_texture.region = cell_coords_to_rect2(0, 2, side_size)
	self.sprite_frames.add_frame("flashRed", frame_2_texture)
	print(self, "\td:", frame_2_texture.region)
	
	var frame_3_texture = start_texture()
	frame_3_texture.region = cell_coords_to_rect2(0, 3, side_size)
	self.sprite_frames.add_frame("flashRed", frame_0_texture)
	print(self, "\te:", frame_3_texture.region)
