extends AnimatedSprite2D

@export var id = ""

func choose_animation(_animation, _start_frame):
	id = _animation
	frame = _start_frame
	play(_animation)
