# Finds matches at the given cell coordinate, in the given cells.

# RMD TODO use Vector2s perhaps? That might contribute to my confusion
# between cell coordinates and graphics coordinates though? 

static func match_at(col, row, cells):
	return(
		horizontal_match_at(col, row, cells) or
		vertical_match_at(col, row, cells) or
		l_match_at(col, row, cells)
	)

# a horizontal match is the same row, different columns
static func horizontal_match_at(col, row, cells):
	if col < 0 or col >= cells.size():
		return false
	if row < 0 or row >= cells[col].size():
		return false
	if cells[col][row] == null:
		return false

	var to_match = cells[col][row].id

	# [x][x][x][ ][ ] 
	if(
		col > 1 and 
		col < cells.size() and
		cells[col-2][row] != null and
		cells[col-2][row].id == to_match and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match
	):
		return true

	# [ ][x][x][x][ ] 
	if(
		col > 0 and
		col < cells.size() - 1 and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match
	):
		return true

	# [ ][ ][x][x][x]
	if(
		col >= 0 and
		col < cells.size() - 2 and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match and
		cells[col+2][row] != null and
		cells[col+2][row].id == to_match
	):
		return true

	return false	
	
# a vertical match is different rows, same column
static func vertical_match_at(col, row, cells):
	if(col < 0 or col >= cells.size()):
		return false
	if(row < 0 or row >= cells[col].size()):
		return false
	if cells[col][row] == null:
		return false
	
	var to_match = cells[col][row].id
		
	# [x]
	# [x]
	# [x]
	# [ ]
	# [ ] 
	if(
		row > 1 and 
		row < cells[col].size() and
		cells[col][row-2] != null and
		cells[col][row-2].id == to_match and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match
	):
		return true

	# [ ]
	# [x]
	# [x]
	# [x]
	# [ ] 
	if(
		row > 0 and
		row < cells[col].size() - 1 and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match
	):
		return true

	# [ ]
	# [ ]
	# [x] 
	# [x]
	# [x]
	if(
		row >= 0 and
		row < cells[col].size() - 2 and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match and
		cells[col][row+2] != null and
		cells[col][row+2].id == to_match
	):
		return true

	return false

# L matches are like, one to my left and one below me makes three
# for all combinations of the 4x4 squares
static func l_match_at(col, row, cells):
	if(col < 0 or col >= cells.size()):
		return false
	if(row < 0 or row >= cells[col].size()):
		return false
	if cells[col][row] == null:
		return false
	
	var to_match = cells[col][row].id
		
	# [X][ ]
	# [x][x]
	if(
		row+1 < cells[col].size() and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match and
		
		col+1 < cells.size() and
		cells[col+1][row+1] != null and
		cells[col+1][row+1].id == to_match
	):
		return true
	
	
	# [x][b]
	# [c][ ]
	if(
		# b
		col+1 < cells.size() and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match and
		# c
		row+1 < cells[col].size() and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match
	):
		return true
	
	# [x][b]
	# [ ][d]
	if(
		# b
		col+1 < cells.size() and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match and
		
		# d
		row+1 < cells.size() and
		cells[col+1][row+1] != null and
		cells[col+1][row+1].id == to_match
	):
		return true
	
	
	# [ ][x]
	# [c][d]
	if(
		# c
		col-1 >= 0 and
		row+1 < cells[col-1].size() and
		
		cells[col-1][row+1] != null and
		cells[col-1][row+1].id == to_match and
		
		# d
		row+1 < cells[col].size() and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match
		
	):
		return true
	
	# [a][x]
	# [c][ ]
	if(
		# a
		col-1 >= 0 and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match and
		# c
		col-1 >= 0 and
		row+1 < cells[col-1].size() and
		cells[col-1][row+1] != null and
		cells[col-1][row+1].id == to_match
	):
		return true
	
	# [a][X]
	# [ ][d]
	if(
		# a
		col-1 >= 0 and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match and		
		# d
		row+1 < cells[col].size() and
		cells[col][row+1] != null and
		cells[col][row+1].id == to_match
		
	):
		return true

	# [ ][b]
	# [X][d]
	if(
		# b
		row-1 >= 0 and
		col+1 < cells.size() and
		cells[col+1][row-1] != null and
		cells[col+1][row-1].id == to_match and		

		# d
		col+1 < cells.size() and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match
		
	):
		return true
		
	# [a][ ]
	# [X][d]
	if(
		# a
		row-1 >= 0 and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match and		

		# d
		col+1 < cells.size() and
		cells[col+1][row] != null and
		cells[col+1][row].id == to_match
	):
		return true
		
		
	# [a][b]
	# [X][ ]
	if(
		# a
		row-1 >= 0 and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match and		

		# b
		row-1 >= 0 and
		col+1 < cells.size() and
		cells[col+1][row-1] != null and
		cells[col+1][row-1].id == to_match
	):
		return true

	# [ ][b]
	# [c][X]
	if(
		# b
		row-1 >= 0 and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match and		

		# c
		col-1 >= 0 and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match
	):
		return true
		
	# [a][ ]
	# [c][X]
	if(
		# a
		row-1 >= 0 and
		col-1 >= 0 and
		cells[col-1][row-1] != null and
		cells[col-1][row-1].id == to_match and		

		# c
		col-1 >= 0 and
		cells[col-1][row] != null and
		cells[col-1][row].id == to_match
	):
		return true
		
	# [a][b]
	# [ ][X]
	if(
		# a
		row-1 >= 0 and
		col-1 >= 0 and
		cells[col-1][row-1] != null and
		cells[col-1][row-1].id == to_match and

		# b
		row-1 >= 0 and
		cells[col][row-1] != null and
		cells[col][row-1].id == to_match
	):
		return true
	
	return false
