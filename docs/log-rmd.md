# log-rmd

## what progression looks like?
20230326 1334 just wrote this down there:
oooooh man okay, so coloured versions of the bricks appear when you get certain combos and shit. they can (maybe?) be matched in any combination with each other but perhaps there's stuff like secondary colours must be created when two primary colours are combined (not sure what happens with all three. that could be white. maybe everything starts grey and then the colour kind of flickers on at some point? ohhhhhhhhhh ohhhhh hthat might be kind of cool and then eventually the player's board is all white and always white and that's some progression. 



## TODO
- performance stuff, if it just runs it gradually consumes more and more ram. slowly but definitely flawed. not destroying the pieces properly or something
- drag pieces visualized
- selected piece visualized

### further
- larger patterns like big squares and stuff

## Log
20230225 2141 relax three is one I've had some thoughts about for a while. but I am still working through the 2d tutorial I guess. I want to noodle and figure it out but I don't even know how to setup the viewport so I'm just going to follow along until I get at least that far I guess

2149 so got to here and the scene just plays fine https://docs.godotengine.org/en/stable/getting_started/first_2d_game/03.coding_the_player.html

can move the player around easily.

so can I noodle from this point? 

20230226 0644 I couldn't right then. did a little bit more tutorial following and this morning I think I can.

I want to:
- make spawners along the top
- spawners create a box
- box drops to bottom of screen

I don't necessarily need physics for this, implementation might be easier but really I don't even think I need to worry about that so much maybe?

the physics stuff means that I get a lot of behaviour for free. make a thing and it falls. destroy a thing and the one above it falls. my impression is that there's relatively simple physics fairly readily available so it's worth trying there

https://docs.godotengine.org/en/stable/tutorials/physics/physics_introduction.html

RigidBody2D is the physics-y things

0723 I mean it's not just "make a box and it drops" which is what I was hoping it was, and there isn't a decent tutorial for "make a box and it drops". like that's all I want to do right now.

0725 there is something about the relationship between an area2d and rigid bodies probably?

20230318 0916 pst-pst Reboot!

I mean there wasn't really much there before but Godot 4 has been formally released and I am starting over in it. I am taking a slightly different tact in that I'm going to draw the background and then do some things from that.

so I've set the viewport to 480x720. Typical portrait mode stuff. so how many boxes do I want to do matches for? 

if a box is 10x10 that would be really small probably? 48 x 72 would be the maximum. I mean why have a border, now that I think about it? 40x40 is 12/18 that's probably a good target. hud can be disclosed with a tap or something maybe? the point of it is that it's all match three so the whole thing is the playing field. 

ugh, I do need to go through that tutorial. at least understand its logic. 

1005 okay that's been really helpful so far actually. Some other stuff too but now I have a piece that falls from the top to the bottom of the screen. So if I put a collider on the bottom and make the pieces themselves colliders. or somethingl ike that?

1105 okay making good-ish progress. I'm sorting out a way to create all the squares in the game programatically so flailing around with MeshInstance2d and such. I realized that I hadn't made this project a git repo yet even so I just ditched the `foolin` project that the earlier attempt was in. there isn't anything of super value reference-wise, I'm already much further along.

I'm duplicating some things but I think they're like all getting the same mesh instance 2d or something?

oh I think green and blue didn't have their scale's set?

1108 huh, that is not the only problem. 

1130 okay lots of stuff closer, gride rendering fully but I guess I got my x and y swapped inthe vector? Or rows/cols or something? rendering wide not tall.

1132 okay, grid renders correctly. I should read my rows/cols logic or something to sort out why it seemed backward? Or it'll just be something eventually. 

that tutorial goes deep, wow. I'm on part 4 of 27, but a lot of that is like, bomb types and other tools that I'm not too much worried about initially. Just want to get my game going. 

so I need to destroy pieces when they're touched, drop the pieces down and get new pieces in to fill. that's several videos still, though my stuff is a lot more simple right now without the sprites or anything like that. 

1136 I'm going to do dishes soon. I think I can switch to gradianttexture2d to get the 

1138 no, I don't think I can actually. It will be worth switching to sprites eventually probably but I would enjoy doing it all programatically. But I want a line outline maybe that pulses and stuff or something so it's probably easiest just to make them in krita.

1140 oh maybe. there are ways to add additional points to the gradient so I might be able to find something that gets close to it? tough to say, worth fooling with. 

after dishes and kitchen shit though. get at least that done. and I need to fucking do taxes today too. Lots of time for playing with these things, get the work shit out of the way today too though!

1257 okay, kitchen done. Oh I should've sorted out some lunch for myself. 
1301 nothing jumps to mind for lunch though so I'll push it a bit. 

1303 so I guess I keep watching the tutorials

1306 the tutorial draws from bottom left and up the column, so it always checks down and left to see if there are matches. does mine draw in the same way? He did width then height, I did rows then cols I think? 

Right so I start top left, draw left to right, go down a row, draw left to right. So I need to check left and up.

so it's just check the elements beside it in the array. 

1325 wow, my focus is just completely fucking shattered by a child slurping on blueberries holy fuck it is widly disruptive to me right now. In part because I don't fucking get what I am rtying to do either. 

I am trying to test for horizontal matches. But there needs to be shit there to test so I'm trying to guard it. 

1449 okay, I have a bit of logic drafted. 

First, I want to change things such that a column is the internal array, right now I have it so that the row is. If the column is, then when a block is destroyed then all the blocks prioer to it can be shited by one element in the array, and then any nulls at the top can be filled with the same fill logic. 

then I think that testing for a vertical match in cell(row, col) is:
```
  column = rows[col] 	# get the vertical column of blocks
  if column[row-2] == column[row-1] == column[row]
    or column[row-1] == column[row] == column[row+1]
    or column[row] == column[row+1] == column[row+2]
```

horizontal is the same, but testing across the row. 

So I need to find all the blocks  that match.

In fact, I'm not going to dispense the right colour! I'm just going to set this up and see what happens. In theory it should keep destroying matches until it finally settles into a stable board?

and if that takes too long then I will worry about dispensing logic. 

So I'm going to do things a little differently as well maybe? 

1519 interuptions a plenty for quite some time. I have something that outputs the addresses of each of the cells in the grid.

1544 It is almost as natural to write (col, row) as it is (y, x), which is to say: Abhorrent. So much so that first time through I wrote it as (row, col)

But excel sheets are col (a-z etc) and row (1..N), so.

1549 so, starting at the bottom left when finding ones to destroy. sure lends some weight to tutorial guy going bottom up, bottom up where I'm going top down, top down. I'm going to stick with this for now because I'm just goingn through the row arrays backwards and that seems fine? perhaps? 

1551 actually it might. I am populating top down, and destroying bottom up. I'm not sure if those being opposites makes sense but it looks neat, so. 

so to do a horizontal match I want to look at the same row in different columns. A vertical match is the same row in one column.

So names are like `@Green@193`

1914 okay, in for a bit again. not terribly long, but interruptions this evening will be short. 

So I want everything between the @s. What string shit does gdscript have?

oh apparently I'm starting at top-right (11, 0) is the top-right square. that's not what I want at all? I don't know that it matters much I'm scanning the whole thing anyway so fuck let's keep going. this works. for now?

until I know why it doesn't, at least.

2015 hmm. It works-ish. there are some single pieces removed which doesn't seem like it should be correct.

ohhhhhhhh because there are problems.

Also I'm not detecting matches that are like:
```
[x][ ]
[x][x]

[x][x]
[x][ ]

etc, all the rotations of those. l-matches I guess. 
```

2016 so I do want to add the things to be deleted to the to_delete array or whatever, and then clear everything in that array out and then loop until the columns are all refilled

2030 it might be interesting to implement infinthree/relaxthree/whatever in that multiplayer rust engine. just a big screen full of things to match three and several players interacting with it at the same time? that's definitely infinithree. it just keeps running and you can join in if you like. 

2035 do I want to do l-matches first or falling? falling gets me to a game sooner but l-matches are quick and the completionist in me is leaning that way I guess.

2238 I'm making like, extra things in the row. row.size() is getting into like the 40s and shit. I'm not erasing the things properly and stuff.

20230319 1010 alrighty ten!

So I did l-matches and falling but weird stuff is afoot with how some row index has like no limit?

1011 so the app halts when trying to destroy one of the pieces.
1013 so this is output right before the halt:
`Blue:<Node2D#30903633467>:<Object#null>`

the null object is the parent of that node2d instance.

```
Column AA : [Blue:<Node2D#30903633467>, @Blue@179:<Node2D#29561456112>, @Red@180:<Node2D#29595010546>, @Green@181:<Node2D#29628564980>, <null>, @Blue@183:<Node2D#29695673848>, <null>, <null>, <null>, <null>, <null>, @Blue@189:<Node2D#29897000452>, @Red@190:<Node2D#29930554886>, @Blue@191:<Node2D#29964109320>, @Green@192:<Node2D#29997663754>, <null>, <null>, <null>, Blue:<Node2D#30903633467>][19]
tpBlue:<Node2D#30903633467>:Grid:<Node2D#23404217446>
tpBlue:<Node2D#30903633467>:<Object#null>
```
it's the 19th one in the list (`[19]`). but how does it get there?

1016 so what I need to find is the cycle before that? those are the only outputs that include it. so maybe if I output the pieces I create ad hoc I can get a bit more?

1017 oh oh oh  `row.append(piece)` I think this is probably wrong. This is a replacement, it doesn't go at the end. that's cut and pasted from the board setup logic. `row[0] = piece`?

1019 yep, I think that was it! it has been running for a while. doesn't seem to be filling up much, this notion of letting the board fill in before the player starts might be fraught. It is still very slow right now though, so I'm going to speed it up a touch. That might be a good ui something to expose, a speed slider.

1023 have had it running for a while, and it does look like memory consumption is ticking its way up 104.6 megs now, but was 103 when I started watching. I'm not sure what sort of profiling options are available but reaching this point with that rust multiplayer thing would be its own kind of victory.

1024 anyway, this is the same run and it has not reached a point of stability. it never will if the board doesn't get setup such that there are no immedate matches. 

1025 107.9 megs now, really chewing things up. not enough things are getting freed from memory. 

1026 okay stopped it. good commit point. next I guess make it so the board can stabilize. something to consider is that the size may be too small or something, it might be more stable with fewer rows and columns.

that's an easy thing to try now I guess.

1028 been going for perhaps a minute, not stable. 96.7 megs of ram
1029 97.6. not stable.

1029 so the tutorial tested for matches when creating the pieces and picked something different if it would. so I'll do that.

1045 I need to break things apart eventually as well, and the pieces can definitely be abstracted better. there's a way to do that programatically but it isn't core for the mechanics so I am letting it wait. The mechanics are very close though, and then the rest of it gets to be a framework for exploring other aspects of game development? I don't know, maybe. sprites and shit. but this is a packageable thing, it's easy to understand and a market exists for it. 

probably there's a way to modify complexity? right now just complete randomness probably isn't either end of the slider but there's a lot of ways to impact piece selection that I will not explore for some time.

1048 okay, but for now: a stable starting board.

1058 okay, because I'm loading into the array instead of initializing it first, it is never big enough to be checked

1105 this is what is kicking out the match during population:
```
	if cells[col][row] == null:
		print("hma 3 ", col, " ", row)
		return false
```

the piece should be in there at this point. Oh but of course it isn't, I'm making a row and appending in there still. with the grid initialized I can just insert right in.

1111 the next thing to implement will be swapping pieces so matches can occur. And then I'm going to have to improve piece creation again probably because random piece selection, I am guessing, will trigger an infinite matching situation again.

I'm guessing. 

So click a piece and the piece has to change to indicate it. Maybe like a new "selectedpiece" gets rendered on top of it or something? does 2d stuff have z layers? these are box meshes in theory but a box mesh with z 0 is a rectangle so anyway. I can figure something out. 

ideally I'd like to change the shader? That seems like the sort of thing that should be common? 
but I know fuck all about shaders, so..

1114 I don't remember how to map the event listener for the click into World again. that's in the godot tutorials though

1117 tried to remind myself inside the interface, could not.

going to use what the tutorial says, in theory that's the most recent guidance?
maybe there is something in best practices.

1124 okay, there's always stuff listening for input events, the key binding stuff is just for more complicated - oh would this mouse shit work on a touchscreen? That's the complexity the key binding stuff allows for.

so do that. 

1128 okay. there's a few ways to do this and I don't udnerstand all the implications but I think the key binding makes things less platform dependant and doing it in the `_input` method keeps this project a little less messy because it doesn't need to be in `_process`.

so I need to translate from x 0 - 480 y 0 - 720 to x 0 - 12 y 0 - 18

so that's div by side I suppose? probably that simple.

1137 okay, I can get the piece, now I want to do something to indicate that it has been selected. Maybe I can add something to it? I don't know.

Maybe the grid can add something to it. the piece has no knolwedge of events or any of that. Can the grid toss something there?

Maybe a "Selected" piece just like the others and it gets swapped in?  `replace_by`

1145 or can I just draw something on top, what happens if I do that? replacing seems overly complicated if it isn't necessary - though I guess maybe when I do the swap something like that will be helpful? 
1147 okay, going down that rabbit hole. swaps seem more complicated than I want them to be but that's okay.

one thing might be to rerender the grid and reparent all the nodes or something like that? so like the grid holds all the pieces but no changes to the pieces happens, and then it just sets all their positions based on where they are in the grid?

that might be an okay way to separate things eventually. I don't think it is needed now but perhaps if I over complicate the swap stuff that will work to simplify it.

1149 so I am... swapping. selecting something. drawing something over top of the selected thing.

1153 okay, toggle visibility. right now I'm created a highlight box as a blue piece but I'm going to make a special object for it and everything, and I'm going to instantiate one in the game and then this will just move it around.

1157 note to self: I am not doing things with the shaders any more I am pretty sure. GradiantTexture1d is the thing I want to manipulate, if I want to manipulate something.

1159 okay, lunch and taxes. no more putting it off! end of the line! 

When I return: I have created the Highlight piece. It is yellow and visible. I want to make the clicked thing take that Highlight piece and move it to the correct spot and make it visible. It should just jump around.

and if the same space it is on is clicked, then it just becomes invisible again and nothing is selected. 

then after that behaviour is in place I'll need to test if things are beside each other. I could add collision squares but probably just testing if the edges are the same is fine? or like, if the two clicked ones are beside each other in the grid right that should be relatively smooth sailing. 

1432 did some tax work, yay me!

1433 so, highlight piece behaviour.

1442 okay, highlight piece can be moved around. it can't be dismissed yet. I need to modify the way I think about cells and use more Vector2s.

1447 if 

okay, cell selection logic with a vector instead of an explicit xy is throwing my brain off I guess.

selected_v

1455 okay, doing swap cells. The node tree doesn't matter but the pieces need to swap?

1501 so just swapping the values in the cells array does not change what is rendered. that was somewhat as expected? I guess I need to position them into their spots. 

So if I do what I thought about above, and think of the grid as something entirely separate from its rendering, and then just redraw the grid every frame or whatever then maybe something interesting is there? 

1503 I should reposition before doing the match at probably?

1509 okay I might be doing that immediately before I try to determine if there's a match there. I'm thinking that's wrong though I might need to position all of them before looking for matches because perhaps something hasn't resolved yet?

1516 I get stopped up by things not working but it's because I've got the time ticker thing set to like 60 billion seconds between turns 

1518 okay okay okay! So now I have swapping pieces implemented and in this case, it immediately triggered the never-ending matches. So my piece selection logic is going to need to be .. better.

1520 I think I have an alarm going off around 3:30 

trying a smaller grid first.

1520 yeah, small grid goes infinite matches just as quickly as the larger.

1550 I don't have any substantial thoughts about how to prevent the autoplaying. 

1552 maybe the problem is lack of colours? the tutorial has 5. maybe more colours will complicate things enough. Worth trying. 

1558 okay if I'm going to start doing colours, and I would like to I think, I need to genericize piece better.

1559 I'm going to start with a basic piece that I can set the color from any color value I guess.

1614 it isn't getting created with a meshinstance2d. galdangit. So this piece scene is becoming irritating .

1615 oh because I was instantiating the script probably not the scene? deleted the scene, just going to stick with the script. See if I can't get the shit to load programatically how I want it to.

1651 alright, well.. I'm pretty happy with this!

1719 cleaned stuff up. played a bit more with the 2d gradient. I don't understand them in the way I'd like, and I don't understand how they are being applied to the cube either, actually. The whole thing does not go on the cube, only a portion of it is visible.

2116 it's like, done! sort of. I mean, there's a ton that can make it better but mechanically it is an infinite match three game. 

2119 I'm not sure what to do next. Visually it isn't awesome. I have the gradients and I could make different things with them and stuff but kind of so what? 

They do need to have different visual markers and stuff. 

what are the different texture types and can I do anything interesting with them? Probably not. 

Probably animation stuff all counts on sprites. Or like, that's the most common - probably because it's known good and reliable and better than anything I"d come up with. But anyway, what kinds of textures are there? There are ways to make shaders do what I want maybe? probably but I don't really get shaders. I guess they create the initial visual state but can one animate with them? Or can they themselves be animations? 

20230320 1850 some thoughts, not clear ones. 

I've been fooling around with the different types of meshes. I'm not somewhere I can keep watching the videos. 

1858 I'm not sure what I want to do with this. the mechanics are in place, now things are all about feel. which means animations, really? I can't read about that on my laptop right now because I am not going to share my data from my phone. Things will be moving soon and I'm sure I'll be able to figure out some other things to do? I don't know. 

20230322 1951 I think there might be another way to do the matches. 

Or it might build on what I have, I'm not sure. I think I want to be able to follow the chain of a match perhaps? 

So the fun of this and I guess the exercise and learning opportunities for me in this are around the interface and interactions and animations and stuff and in a match three game there's like combos and bonuses and multipliers and fireworks and stuff right? like pepple level excitement is what I'm aiming for but I don't think I"m going for peggle style visuals.

I guess in theory different skins for it might be fun, and an in-game purchase actually, yeah that's a good in-game purchase. 

But it all depends on a lot of stuff. Perhaps I could even _plan_ something. 

1955 Objectives are okay, but the objectives are always available, aren't recorded and aren't explained. Like.. I don't know what an objective is. But a lot of match three games have levels right? but I guess a lot of those are layouts. 

maybe there are like .. moves? like a hat trick is an animation, just the text spinning out towards the camera or whatever, but it only shows up if you do trigger the disappearance of three groups of the same colour somehow. 

Tracking stats might be interesting but that's still explicit progression and I'm not sure I want that around. Perhaps stats tracking is off by default or something like that, if a lot of people want it even. 

1958 I .. ugh, implementing the drag and drop swap will be irritating probably? I guess it's sort of an elasticy feel, the dragged piece moves in the direction of the mouse/finger/whatever.  So if like the player drags straight up to 12 o'clock and then rotates the drag point around the piece clockwise, say, it starts swapping with the piece above it and then like.. sometime maybe around like 2 o'clock or something it swaps back and then to the right piece, then sometime perhaps around 5 it goes down, and around 8 it go left, 11 back up etc. 

maybe there's a dead zone between each cardinal too. like between 1 and 2 is probably too big, but something like that. 

and I do really need to understand animations and settle in on something. They aren't that hard and actually I could just do like a 40x40 square where like 6 rows down or something it's a bright white line and then it steps down in 2 or three blurs. And I might be able to apply something to this base sprite to change the color or something perhaps? 

20230324 2215 been experimenting with sprites. have some interesting looking square outlines that would do well to be colored. 

What if I put a red mask on them or something? 

20230325 1218 I got them into a nice bright place. Visually distinct and strong colors. Possibly even some frames for animations, though I'm not 100% certain.

So I'm going to need to make a sprite sheet out of this giant image I made I guess? Vector graphics preferably I think?

1532 okay, making pretty okay progress on getting an animated sprite in. The problem is it doesn't have any of the customized Piece logic I have. which I guess is mostly storing color. But I need to make a thing.

1602 probably overthinking things but whatever, rolling along.

```
func cell_coords_to_rect2(cell_x, cell_y, side_size):
```

is what I'm trying to sort out. So this is for the sprite sheet. Each image is 40x40. 

(0, 0) -> (0, 0, 40, 40)

(0, 1) -> (0, 40, 40, 40)

1605 oh hey, it's always width height, that makes it a matter of just worrying about the xy position. 

1734 I can't make it visible. 
1738 Grid's visibility was off. I wonder how long I've been chasing my tail because of that. (Some of the gap between these entries was supper prep)

1740 didn't immediate resolve the problem and now it is time to cook the prepped things

2219 okay, so I tried - this is going okay. I tried setting each piece's frame 3 to have a random duration but I'm changing the animation that they're all using, so all pieces with the same animation glow at the same time.

This is not entirely what I had envisioned. 

2220 but new ones do all have their own time frame.

2225 a bit of animation in there is nice but I don't know that it adds to the effect. I don't understand why new ones have their own time frame. I guess they're instantiated far enough apart. The animation is not globally synced so their 60 second duration or whatever starts at a different time.

2228 so what if I populate the grid for the first time by dropping them in from the start, can I do that? 

2240 so I think some good things to do:
- do the piece drag behaviour
- toggle somewhere or how or something to switch to the color swatch or something perhaps
- particle effects when things disappear?

2241 the dragging thing is something I want for the phone. also I want the pieces to be bigger probably? which suggests an svg so that can be a setting and things can scale. I should use the other tool, not illustrator but the oss replacement.

20230326 1053 alright, the drag thing is the thing. that's the thing that's currently blocking me from wanting to try putting it online. that's what I tell myself at least. it isn't something to crow about really but I do like it. And I do want to make it take up the entire viewport it is provided with a range of sizes. a little settings pane somewhere or something

double tap anywhere to bring up the menu with board settings. just let people fill the space themselves? I don't know. it seems unlikely that this engine can do dynamic viewports but maybe it can?

1055 the equality check on swap isn't working properly. that's a small thing I can do. the drag thing is a big thing that I don't know the entry point to just yet. 

1105 this is the first thing I've built that is genuinely fun to interact with. imtg has some hints but desperately needs a workable interface for it to be even a little bit visible, ftree is a deeply enjoyable system to make but there isn't interaction with the thing itself really, but this is just fun. Which is exactly what I want! 

anyway I came here to note a possible feature: magnification around the cursor. the tile that it is over pulls forward a bit and increases in size by like 5% or something but also the 4 cardinal direction tiles do too so it's easy to see neighbours

but again, scaling things means scalable images, need to redo with svgs. so get the swap test working again and work on drag and drop and then so many more fun things can be done!

1108 oh there is no criteria in code for the swap

1110 that's fine for now then I guess. do the drag and swap. So some steps:

- detect drag
- figure out which direction is being dragged
- update direction being dragged as drag continues
- get tile to be swapped with
- swap tiles on release
- leave tiles if there is no direction

1257 rewards for larger patterns in the tiles. All the angles and crosses and stuff, if someone makes a big square or something like that. 

1259 oh man, have the paths light up with energy or something. lots of cool effects that could be done. in the future! when other things are in place. not least of why because I don't think I can make it happen with the level of knowledge I have and the tech in here and several things like the graphics and structure. need to decompose this giant grid class some way or another.

someday

1302 so I have drag detection. next is direction detection.

there's the relative property, which outputs a vector2, eg: 

```
dragging?(0, -6)
dragging?(0, -12)
dragging?(0, -4)
dragging?(0, -2)
dragging?(2, -8)
dragging?(0, -6)
dragging?(0, -2)
dragging?(0, -10)
dragging?(0, -2)
dragging?(0, -2)
dragging?(2, -6)
```

I was mostly dragging up, so y was going negative. This thing I read https://www.reddit.com/r/godot/comments/garic2/directional_screen_drag_input_with_godot/ suggests that a good threshold is if the x or y in relative is > abs 3. so y < -3 means up,  y  > 3 means down. worth starting there and seeing what happens

```
	if(relative.y > 3):
		dragging_down = true
	else:
		dragging_down = false
```

doing it like this for them all. not sure I should be setting these to false but I think maybe? we'll see.

1311 so perhaps I don't want to compare against relative. What about original position/click position vs current position?

1322 so I think the challenge here is that when the mouse stops moving the drag stops being recognized. 

also probably that I am working with pure x/y coordinates, not cell coordinates. 

So translate the xy to cell.

1323 okay but first try extracting the match stuff.

1332 oooooh man okay, so coloured versions of the bricks appear when you get certain combos and shit. they can (maybe?) be matched in any combination with each other but perhaps there's stuff like secondary colours must be created when two primary colours are combined (not sure what happens with all three. that could be white. maybe everything starts grey and then the colour kind of flickers on at some point? ohhhhhhhhhh ohhhhh hthat might be kind of cool and then eventually the player's board is all white and always white and that's some progression. 

1335 I'd also like to make the svgs much more flexible, get them tinted with the right color instead of having a bunch of specific colours? probably perhaps? 

1336 I have little tingles from this! I like little tingles! I'm over excited about it but that's also the point of all this. Alright, so keep on with the drag stuff. 
1337 leet! For the drag stuff the thing I want to do is work in cell coords, not graphic coords. So get on that. 

```
static func graphic_coord_to_cell_coord(vector2):
	return (0, 0)
```
that doesn't work, of course. hmm.

1343 I'll return a Dictionary for now. Best option. 

1349 I might need corner pieces. I am certainly distractable with this drag thing. 

So okay. Map the thing.

1406 okay! so that's getting there. But the translations between rows and colls and xes and ys and shit is mixed up

I need to do some house things. When I get back, the output in the thing says up is true but it should have been left is true, so the direction detection is off

1407 or something. the direction is off, but closer!

1608 oh, the mapper logic is just returning x, not modifying it.

1611 Mapper needing a side length suggests an instance might be better. ah well.

1640 I have some swapping in place but the selected stuff might be messing with it as well? Perhaps selected should only get set on release? Unclear. 

1642 holy smokes, yes that was part of the problem!

1644 okay, so if I have drag swapping enabled I can't have select swapping. 

Selection should only happen if the mouse is released on the box it was clicked on.

1647 that turned out to be relatively simple. If no swap occurs, then it is selected. 

1908 so drag works, but there's no indication to the player

2056 so, selected can be a thing and the swap has to do something

right now the swap doesn't work great because of reasons. one of them is that it does the real swap so when the match detection goes off, there it goes. 

maybe I can clone the pieces and scale them up a bit or something? what happens if I just scale up the piece by 20% say?

2101 multiplying by 1.4 works quite nicely, actually. 

2202 line 114 - this is where they get swapped when I'm doing the dragging. instead I can animate the pieces into each other's positions and not do the swap until the selection ends. 

that's next time's task. I hope that is coherently communicated

20230328 2105 swap. animation. tween. things like that.

2115 oh getting close! it isn't working because it gets called a billion times during the process, so the starting positions change every time until they meet in the middle.

So it needs to get set once during the drag or determined from its position. that might be better.

2119 closer yet! need to offset them, /2 _ length somewhere?

2124 okay so I think what happens is that the drag works until the tick redraws the tiles back in their original location, which is pretty quickly. 

2125 so this may need to be more elaborate than I'd like. messing with the position of things in the grid doesn't work, because the grid gets redrawn all the time. 

So maybe I can make dummy objects that get swapped? If there's only ever two possible things, then the release can just clear them away, stuff like that? 

2129 alright, probably done for the night here. I did a quick test with duplicate objects but it didn't work so I undid/ctrl-z'd it all back to this state so I can commit it. Not there yet, but good steps.

20230331 2045 okay. yeah it doesn't work great right now and its irritating enough that I am glancing back at this. not sure I have the brain for it right now though. 

2101 Made a bit of progress with duplicates. Swap happens but unswap doesn't and dupes for the other neighbours don't get created and stuff doesn't move when the drag changes

2102 I need to make the duplicates class members I think.

2131 I think I am making a bunch of duplicates of the same thing and orphaning them perhaps

2144 when releasing they don't need to go back, this isn't a reset if no match thing, it's a swap whatever pieces you want thing

20230401 0914 the first thing to work on is having them return to the neutral location when appropriate. 

0922 so I have a visually swap back (lovely naming) and it should tween the swapped things back. I guess it can just reject nulls

0924 I think the clunkiness of the logic is part of why it is so difficult. There isn't really flexibility in these actions so they don't flow naturally together. I think I'm approaching that level of generalization but it's one that's going to take a series of small things to get to. 

0927 I think I might move on to scalable images before messing around with the dragging too much more. 

if the drag is only ever one direction it is fine. that's not actually functional but it is representative enough of functionality for me to be okay with moving on. 

I think that the scalable images are important enough to me to do before publishing? But I want to think on that a little bit more perhaps. 

publishing probably isn't heavily involved. and I'd like to try it on my phone.

0931 so many caveats for web publishing. doesn't work on macs or ios at all I guess? and not on firefox on linux. 

well, let's see what happens anyway. one export and opening it on the browser is easy enough to try

1047 did a bit of yardwork. tried servering pages now, just opening and with `http-server` and got errors about the things the docs suggested I would get errors about. So, that's a real bummer for me.

There isn't really anything stopping me from doing this in any javascript game engine sort of thing though, this logic is moderately portable to web stuff, assuming it has those same conventions. 

1052 hmm, pixijs is giving me thoughts. https://pixijs.io

use similar sort of tech to game engine conventions to build a gamier interface for imtg perhaps. lose a bunch of that svelte work but if there's a community around this pixijs then that might be okay.

1156 its simple graphics are of interest to me. although svgs of these patterns could be interesting as well.

>  PixiJS doesn't actually draw anything. Instead, it stores the rectangle you "drew" into a list of geometry for later use. If you then add the Graphics object to the scene, the renderer will come along, and ask the Graphics object to render itself. At that point, your rectangle actually gets drawn - along with any other shapes, lines, etc. that you've added to the geometry list.
>  

right? so it's all about composing visuals programatically. I might adore this!

20230504 2044 I am getting into rust and migrating imtg so this is likely not going to see movement for a while. maybe after safari and godot can figure their shit out. 

anyway, this wasn't hosted anywhere! So now it will be: git remote add origin https://codeberg.org/rmd/relax-three.git


